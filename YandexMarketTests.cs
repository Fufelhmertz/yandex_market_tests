using System;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Remote;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Threading;

namespace YandexMarket.Test
{
    [TestClass]
    public class YandexMarketTests
    {
        IWebDriver Browser;

        [TestMethod]
        public void Test_Search_MobilePhone()
        {
            Browser = new OpenQA.Selenium.Chrome.ChromeDriver();
            Browser.Manage().Window.Maximize();
            Browser.Navigate().GoToUrl("http://yandex.ru");

            //Open Market
            Browser.FindElement(By.CssSelector(@"a[data-id='market']")).Click(); 

            Browser.SwitchTo().Window(Browser.WindowHandles.Last()); 
            
            //Opem Electronika        
            Browser.FindElement(By.CssSelector(@"a[href='/catalog--elektronika/54440']")).Click();

            Browser.FindElement(By.XPath("//a[contains(@href, 'catalog--mobilnye-telefony')]")).Click();          
                        
            Browser.SwitchTo().Window(Browser.WindowHandles.Last());

            //Open Advanced Search
            Browser.FindElement(By.XPath("(//a[contains(@class, '_2EPSjI-GdM _1U1dDjJdGe ')])[8]")).Click(); 

            //Price
            Browser.FindElement(By.Name("glf-pricefrom-var")).SendKeys("20000");

            //  Apple
            Browser.FindElement(By.CssSelector(@"[for='glf-7893318-153043']")).Click();
            // Samsung
            Browser.FindElement(By.CssSelector(@"[for='glf-7893318-153061']")).Click();

            // Result
            Browser.FindElement(By.XPath("//a[contains(@class, 'button_action_show-filtered')]")).Click();

           
            //Test Failed, because there are 48 items on the page. (must be 12)        
            var actualElements = Browser.FindElements(By.XPath("//h3[contains(@class,'n-snippet-cell2__title')]")).Count();
            var expectedElements = 48;
            Assert.AreEqual(expectedElements, actualElements);

            //Save first product name
            var phoneName = Browser.FindElement(By.XPath("(//h3[contains(@class,'n-snippet-cell2__title')])[1]")).GetAttribute("title");
            Browser.FindElement(By.Id("header-search")).SendKeys(phoneName + OpenQA.Selenium.Keys.Enter);

            
            var resultValue= Browser.FindElement(By.XPath("(//h3[contains(@class,'n-snippet-cell2__title')])[1]")).GetAttribute("title");

             Assert.IsTrue(resultValue.SequenceEqual(phoneName));

             Browser.Dispose();
        }

        [TestMethod]
        public void Test_Search_HeapPhone()
        {
            Browser = new OpenQA.Selenium.Chrome.ChromeDriver();
            Browser.Manage().Window.Maximize();
            Browser.Navigate().GoToUrl("http://yandex.ru");

            //Open Market
            Browser.FindElement(By.CssSelector(@"a[data-id='market']")).Click();
            
            Browser.SwitchTo().Window(Browser.WindowHandles.Last());

            //Opem Electronika           
            Browser.FindElement(By.CssSelector(@"a[href='/catalog--elektronika/54440']")).Click();

            Browser.FindElement(By.XPath("//a[contains(@href, 'catalog--naushniki-i-bluetooth-garnitury')]")).Click(); 
            
            Browser.SwitchTo().Window(Browser.WindowHandles.Last());

            //Open Advanced Search   
            Browser.FindElement(By.XPath("(//a[contains(@class, '_2EPSjI-GdM _1U1dDjJdGe ')])[8]")).Click();

            //Price
            Browser.FindElement(By.Name("glf-pricefrom-var")).SendKeys("5000");

            // Beats
            Browser.FindElement(By.CssSelector(@"[for='glf-7893318-8455647']")).Click();
            

            // Result
            Browser.FindElement(By.XPath("//a[contains(@class, 'button_action_show-filtered')]")).Click();

            //Test Failed, because there are ~27 items on the page. (must be 12)    
            //  var actualElements = Browser.FindElements(By.XPath("//h3[contains(@class,'n-snippet-card2__title')]")).Count();
            //  var expectedElements = 48;
            //  Assert.AreEqual(expectedElements, actualElements);

            //Save first product name
            var heapPhoneName = Browser.FindElement(By.XPath("//h3[contains(@class,'n-snippet-card2__title')]")).GetAttribute("title");
            Browser.FindElement(By.Id("header-search")).SendKeys(heapPhoneName + OpenQA.Selenium.Keys.Enter);
            
            var resultValue = Browser.FindElement(By.XPath("(//h3[contains(@class,'n-snippet-card2__title')])[1]")).GetAttribute("title");
            
            Assert.IsTrue(resultValue.SequenceEqual(heapPhoneName));

            Browser.Dispose();
        }

        [TestMethod]
        public void Test_PriceSort()
        {
            Browser = new OpenQA.Selenium.Chrome.ChromeDriver();
            Browser.Manage().Window.Maximize();
            Browser.Navigate().GoToUrl("http://yandex.ru");

            //Open Market
            Browser.FindElement(By.CssSelector(@"a[data-id='market']")).Click();

            
            Browser.SwitchTo().Window(Browser.WindowHandles.Last());

            //Opem Electronika           
            Browser.FindElement(By.CssSelector(@"a[href='/catalog--elektronika/54440']")).Click(); 

            Browser.FindElement(By.XPath("//a[contains(@href, 'catalog--mobilnye-telefony')]")).Click(); 
                        
            Browser.SwitchTo().Window(Browser.WindowHandles.Last());
            //Sort Items
            Browser.FindElement(By.XPath("//div[contains(@data-bem,'aprice')] ")).Click(); 
            
            Thread.Sleep(2000);            

            //Save all elements on page
            List<IWebElement> sortPrice = Browser.FindElements(By.XPath("//div[@class='price']")).ToList();
            List<int> actualSortPrice = new List<int>();
            foreach(var e in sortPrice)
            {                
                actualSortPrice.Add(int.Parse(e.Text.Split(" ")[0]));
            }          

            
            List<int> expectedSortPrice = new List<int>();
            expectedSortPrice.AddRange(actualSortPrice);
            expectedSortPrice.Sort();

           Assert.IsTrue(expectedSortPrice.SequenceEqual(actualSortPrice));

           Browser.Dispose();
           
        }
    }
}
